#pragma once
#include "PostProcessingShader.h"
#include <iostream>

class BrightnessPP :public PostProcessingShader
{
public:
	BrightnessPP(float strength)
	{
		fragFilename = "Shaders/PP/brightness_shader.shader";
		s = strength;
		programID = setupShaders(vertexFilename, fragFilename);
		setup();
	}
	void update()
	{
		glUseProgram(programID);
		glUniform1f(locStrength, s);
	}

	void setBrightness(float x)
	{
		s = x; 
	}
private:
	GLuint locStrength;
	void setup()
	{
		locStrength = glGetUniformLocation(programID, "brightFactor");
		glUseProgram(programID);
		glUniform1f(locStrength, s);
		glUseProgram(0);
	}
	float s;
};