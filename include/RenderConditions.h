#pragma once
class Shader;

struct RenderConditions
{
public:
	RenderConditions() {}
	Shader* shaderType = nullptr;

	int renderPriority = 0;

	bool containsTransparency = false;
	bool castShadows = true;

};

