#pragma once
#include "Shader.h"
#include <vector>

class ShaderManager
{public:

	void addShader(Shader * x)
	{
		shaders.push_back(x);
	}

	void beginFrame()
	{
		for (int i =0; i < shaders.size(); i++)
		{
			shaders[i]->newFrame();
		}
	}
private:
	std::vector<Shader* > shaders;
};