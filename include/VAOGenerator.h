#pragma once
#include <glad\glad.h>
#include "VAOData.h"
//this Class generates VAOs for any class that needs it, primarily the obj examiner. 

class VAOGenerator
{
public:
	static VAOGenerator * getInstance();

	GLuint makeVAO(MeshData *);
	GLuint makeVAOIndexed(MeshData *);
	GLuint makeVAOInstanced(MeshData *);

private:
	static VAOGenerator* thisPointer;
	VAOGenerator();
	~VAOGenerator();
};

