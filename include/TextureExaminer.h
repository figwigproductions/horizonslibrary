#pragma once
#include <glad/glad.h>
#include <glfw/glfw3.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Texture.h"

using namespace std;




class TextureExaminer
{
public:
	static TextureExaminer * singleton();

	Texture getTexture(string);
	Texture getNormalTexture(string);

	void setDirectory(string x) { directory = x; }

private:
	static TextureExaminer * thisPointer;


	TextureExaminer();
	~TextureExaminer();

	const static int MAX_TEXTURES = 64;
	Texture textures[MAX_TEXTURES];

	int texturesDone = 0;

	void readInNewTexture(int index, string filename);
	void readInNewNormalTexture(int index, string filename);

	string removeLongFileName(string);

	string directory = "";
};

