#pragma once
class Volume;
#include "Component.h"
#include "Triangle.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/btBulletCollisionCommon.h>


struct Force
{
	glm::vec4 magnitude;
	glm::vec3 posRelCG;
	Force(glm::vec3 mag, glm::vec3 pos)
	{
		magnitude = glm::vec4(mag,1);
		posRelCG = pos;
	}
	Force operator * (glm::mat4 toApply) const
	{
		return Force(toApply * magnitude, posRelCG);
	}
};

class PhysicsComponent :public Component
{
public:
	PhysicsComponent(GameObject *, float m  );
	//Hostless
	PhysicsComponent(btCollisionShape *, glm::mat4 transform);
	~PhysicsComponent();

	//perform movement required for this frame based on values calculated by the physics handler
	void btUpdate(double);
	void btLateUpdate(double);

	void applyForce(glm::vec3);
	void applyForce(Force);
	void applyAcceleration(glm::vec3);
	void setMass(float m);

	//Only to be used by physics handlers
	void setHostPosition(glm::vec3); //to be used sparingly.
	//Only to be used by physics handlers
	void setHostOrientation(glm::quat);

	void applyImpulse(glm::vec3);

	float mass = 0.0f;

	bool noHostComp = false;


	bool deleteMe = false;

	//Bullet Stuff
	btCollisionShape* bulletHostless = nullptr;
	btTransform  transform;
	btRigidBody * body;
	btCollisionShape * getBtShape();

	void setupBullet();


	float responsiveness = 100.0f;
	bool bulletIndependant = false;
	bool setup = false;
private:

	bool isBullet = false;
	btDefaultMotionState * motionState;

	glm::mat4 hostlessTransform;


};

