#pragma once

#define INPUT_LEFT_MOUSE_DOWN 0
#define INPUT_RIGHT_MOUSE_DOWN 1
#define INPUT_LEFT_MOUSE_UP 2
#define INPUT_RIGHT_MOUSE_UP 3

class InputListener
{
public:
	virtual void OnClick(int key, float x, float y) {}
	virtual void mouseMovePos(float x, float y) {}
};