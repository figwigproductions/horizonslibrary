
#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

// FreeImage texture loader
GLuint fiLoadTexture(const char *filename);
GLuint fiLoadTextureNormal(const char *filename);

void fiLoadTextureSkybox(const char *filename,int type);


int getH();
int getW();

bool getOkay();