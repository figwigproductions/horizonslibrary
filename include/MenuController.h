#pragma once
#include "GameObject.h"
#include "Game.h"
#include "PublicBehaviour.h"

class MenuController :
	public PublicBehaviour
{
public:
	MenuController(Game* );
	~MenuController();
	void draw(RenderSettings*) {};
	void update(double);
	Volume * getVolume() { return nullptr; }
private:
	Game * refr;

	bool choiceMade = false;
};

