#pragma once
#include "GameObject.h"
#include "PrivateBehaviour.h"
#include "RenderSettings.h"
class Game;
class PlayerController : public PrivateBehaviour
{
public:
	PlayerController(Game * refr);
	~PlayerController();

	void update(double);
	Volume * getVolume() { return nullptr; }
	void draw(RenderSettings* rs);
	void setSpeed(float x) { speed = x; }
private:
	Game * refr;

	float speed = 0.02f;

};

