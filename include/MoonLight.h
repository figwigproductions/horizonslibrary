#pragma once
#include "Light.h"
class Moonlight : public Light
{
public:
	Moonlight( glm::vec3 pos)
	{
		position = glm::vec4(pos, 0.0f);
		attenuation = 0.01f;
		ambientCoefficient = 0.0001f;

		coneDirection = glm::vec3(0.0f, -1.0f, 0.0f);
		intensities = glm::vec3(0.35f, 0.35f, 0.35f);

		castShadows = true;
	}


};


class Daylight : public Light
{
public:
	Daylight(glm::vec3 pos)
	{
		position = glm::vec4(pos, 0.0f);
		attenuation = 0.01f;
		ambientCoefficient = 0.1f;

		coneDirection = glm::vec3(0.0f, -1.0f, 0.0f);
		intensities = glm::vec3(1.2f, 1.2f, 1.1f);
		castShadows = true;
	}


};