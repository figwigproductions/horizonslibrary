#pragma once
#include "Scene.h"
#include "Fade.h"
#include "ImageRect.h"

class SplashScreen :
	public Scene
{
public:
	SplashScreen(ImageRect *);
	~SplashScreen();

	void load(Game*);
	void run();

private:
	Fade * fader;
	ImageRect * img;
};

