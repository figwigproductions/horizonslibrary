#pragma once
#include "GameObject.h"
#include "VAOData.h"
#include "TerrainTextureData.h"
#include "Terrain.h"
#include "Triangle.h"
#include "Bounds.h"
#include "Extents.h"
#include <bullet/btBulletCollisionCommon.h>
#include "tiny_obj_loader.h"

class ObjectFactory
{
public:
	static ObjectFactory * getInstance();

	virtual Terrain * makeTerrain(VAOData * v, TerrainTextureData * t, float tile, float scale);
	virtual GameObject * makeInstancedObject(InstancedVAOData *);
	virtual GameObject * makeCustomObject(VAOData*, glm::vec3 pos, glm::vec3 scale, glm::vec3 rot);
	virtual GameObject * makeCustomObject(VAOData*, float scale = 1.0f);

	virtual void addToTerrain(VAOData*);
	virtual btCollisionShape* createTerrainCollider();
	virtual btCollisionShape * makeBoxCollider(GameObject*);
	virtual btCollisionShape * makeBoxCollider(Extents volumeExtents);
	virtual btCollisionShape * makeCylinderCollider(GameObject*);
	virtual btCollisionShape * makeCylinderCollider(Extents volumeExtents);
	virtual btCollisionShape * makeComplexCollider(VAOData* data);
	virtual btCollisionShape * makeComplexCollider(MeshData* data);

	virtual bool handleCustomPrefix(tinyobj::shape_t shape) { return false; }

protected:

	ObjectFactory();
	~ObjectFactory();

	static ObjectFactory* thisPointer;

	
	btTriangleMesh* triangles = nullptr;



};

