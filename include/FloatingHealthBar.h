#pragma once
#include "WorldGUI.h"
#include "Text.h"
class FloatingHealthBar :public Object3D
	
{
public:
	FloatingHealthBar(GLuint barTex, GLuint progressTex, glm::vec2 res, float portionX, float portionY, bool facingCamera = false);
	~FloatingHealthBar();

	void update(double);
	void draw(RenderSettings*);

	void setup(float);
	void setProgress(float x);

protected:
	GLuint barTex = 0;
	GLuint progressTex = 0;
	float progress = 50.0f;
	
	glm::vec2 barRes;
	glm::vec2 progressRes;

	GLuint barVAO;
	GLuint progressVAO;

	bool first = true;

	WorldGUI * barGUI;
	WorldGUI * progressGUI;

	Text * text;

	float progWidth; 

	float maxProgress;

	bool faceCamera;
};

