#pragma once
#include "Behaviour.h"
class GameObject;
class PrivateBehaviour :public Behaviour
{
public:
	PrivateBehaviour() {}
	~PrivateBehaviour() {}

	virtual void update(double) = 0;
	void setTarget(GameObject * x) { target = x; }
	GameObject * getTarget() { return target; }
protected:
	GameObject * target;

};
