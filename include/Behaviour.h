#pragma once

class Behaviour
{
public:
	Behaviour() {}
	~Behaviour() {}

	void setActive(bool x) { active = x; }

	virtual void update(double f) = 0;
	virtual void setup() {}
protected:

	bool active = true;
};