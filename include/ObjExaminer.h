#pragma once
#include <string>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm-0.9.9.2/glm/glm.hpp>
#include "tiny_obj_loader.h"
#include "Volume.h"
#include <stdio.h>
#include "VAOData.h"
#include "TerrainTextureData.h"
#include "TextureExaminer.h"
#include "ObjectFactory.h"
#include "Terrain.h"
#include "Extents.h"
#include "Helpers.h"
#include "Path.h"
#include "SceneConfig.h"

#define OE_VOLUMETYPE_BOX 0
#define OE_VOLUMETYPE_CYL 1
#define OE_VOLUMETYPE_CNVX 2

class Scene;

class ObjExaminer
{
public:

	static ObjExaminer* getInstance();

	void setFile(std::string newFileName) { currFilename = newFileName; importPoints(); }
	void setCurrShape(std::string newShape) { currShapeName = newShape; sortPoints(false);}

	std::string* getShapeNames();

	int getShapeCount() { return shapes.size(); }
	int getTotalVerts() { return totalVerts; }

	GLuint getVAO() { sortVAO();  return shapeVAO; }


	glm::vec3 getCentre() { return averageTot; }

	void indexShapes(VAOData*, std::string shapeName ="", bool zero = true);

	void indexScene(Scene *, SceneConfig);

	void setDirectoryTexture(std::string x) { textureDirectory = x; }

	void setDirectoryObject(std::string x) { objectDirectory = x; }

	void changeObjectFactory(ObjectFactory * x) { objectMaker = x; }

	Extents getExtents();

private:

	Extents e;

	void deleteBasic();
	void deleteIndexed();
	void deleteInstanced();
	void deleteIndexBuffers();

	static ObjExaminer * thisPointer;

	ObjExaminer();
	~ObjExaminer();

	std::string objectDirectory = "";
	std::string textureDirectory = "";

	std::string removeNumber(std::string);

	std::string currFilename = "";
	std::string currShapeName = "";

	void importPoints();
	void sortPoints(bool, Bounds2DXZ restrictions = Bounds2DXZ());

	void moveToOrigin(bool useMinY);
	void sortVAO();
	void sortIndexedVAO();
	void sortInstancedVAO(int instances);

	TextureExaminer* textExam;

	GLuint getTexture(int type, int shapeNo);// 0 = difuse, 1 = spec, 2 = normal

	int totalVerts = 0;
	int indicesCount = 0;

	tinyobj::attrib_t					attrib;
	std::vector<tinyobj::shape_t>		shapes;
	std::vector<tinyobj::material_t>	materials;

	GLuint vertsVBO = 0;
	GLuint textsVBO = 0;
	GLuint normsVBO = 0;
	GLuint tangentsVBO = 0;
	GLuint bitangentsVBO = 0;
	GLuint indicesVBO = 0;
	GLuint modelMatriciesVBO = 0;

	GLuint shapeVAO = 0;

	Volume * volume = nullptr;

	// the number of vertexes to check for the indexing algorithm. 
	//In very very large objects, this can speed up things greatly
	int indexingAccuracy = 45;

#pragma region Arrays and Associated Variables
	//The const to define size of vertexes

	bool basicAllocated = false;
	float *points = nullptr;
	float *norms = nullptr;
	float *texts = nullptr;
	float *tangents = nullptr;
	float *bitangents = nullptr;

	//Temporary container for indices
	float* indexingBufferV = nullptr;
	float *indexingBufferN = nullptr;
	float *indexingBufferTex = nullptr;
	float *indexingBufferTan = nullptr;
	float *indexingBufferBitan = nullptr;
	int * indexBuffer = nullptr;

	float * indexedPoints;
	float * indexedNorms;
	float * indexedTexts;
	float * indexedTangents;
	float * indexedBitangents;
	GLuint * indices;

#pragma endregion

	int indicesLength;
	int uniqueVertexes;
	bool makingVolume = false;

	glm::vec3 averageTot = glm::vec3(0, 0, 0);
	glm::vec3 minExtent;
	glm::vec3 maxExtent;
	SceneConfig scMain;


#pragma region Instancing Variables
	glm::mat4 *modelMatrices;
#pragma endregion

	ObjectFactory * objectMaker = nullptr;

#pragma region Scene Importing Pipeline
	void addTerrain( std::string shapeName, Scene *);
	void addCustom(int shapeID, Scene *);
	//Create a meshdata from the currently imported shape. Use diff, norm and spec textures
	MeshData* indexMesh(GLuint diff, GLuint spec, GLuint norm, int instances);

	void sortInstancedObjects(Scene*);
	void sortCollisionShapes(Scene *);

	int readShapesCount = 0;
	const static int MAX_INSTANCED = 128;
	std::string readShapes[MAX_INSTANCED] = { "" };
	int repeatCounts[MAX_INSTANCED] = { 0 };
	int ogShapeID[MAX_INSTANCED] = { 0 };

	glm::mat4 makeInstancingMat(std::string, int, Scene * );

	std::string addNumber(std::string, int);

	bool debugTriangleSpheres = false;

	int terrainCount = 0;

	std::vector<int> volumeShapeIDs;

	Extents calcShapeExtents(int shapeID, bool moveToOrigin = false);
#pragma endregion

};

