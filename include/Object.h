#pragma once
#include "ComponentReference.h"
#include "Component.h"
#include "Components.h"
#include <vector>
//Common Parent for any object that can contain a component. 

class Object
{
public:
	void addComponent(Component * x) 
	{
		compStruct.components.push_back(x);
	}

	virtual void setup() {}


	template <typename CompType>
	inline void removeComponent(CompType)
	{
		int counter = 0;
		for (Component * currComp : compStruct.components)
		{
			if (static_cast<CompType>(currComp) != nullptr)
			{
				delete currComp;
				compStruct.components.erase(compStruct.components.begin() + counter);
				return;
			}
			counter++;
		}
	}


	template <typename CompType>
	inline CompType getComponent()
	{
		for(Component * currComp : compStruct.components)
		{
			CompType currentEntry = dynamic_cast<CompType>(currComp);
			if (currentEntry != nullptr)
			{
				return currentEntry;
			}
		}
		return nullptr;
	}

protected:

private:
	Components compStruct;
};