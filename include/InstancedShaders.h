#pragma once
#include "Shader.h"
#include "DefaultShaders.h"
class InstancedShaders :
	public DefaultShaders
{
public:
	static InstancedShaders * getInstance();
	
	void setCameraMat(glm::mat4);
protected:

	std::string vertFilename = "Shaders/vertex_shader_i.shader";
	std::string fragFilename = "Shaders/fragment_shader_i.shader";

	void setup();
	InstancedShaders();
	~InstancedShaders();

	static InstancedShaders* thisPointer;
};

