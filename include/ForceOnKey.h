#pragma once
#include "PrivateBehaviour.h"
#include "Game.h"
#include "PhysicsComponent.h"

class ForceOnKey :public PrivateBehaviour
{
public:
	ForceOnKey(Keystate key, glm::vec3 force)
	{
		k = key;
		f = force;
	}


	void update(double x)
	{
		if (Game::singleton()->getKeys() & k)
		{
			if (target->getComponent<PhysicsComponent*>() != nullptr)
				target->getComponent<PhysicsComponent*>()->applyForce(f);
			else
				std::cout << "the attached object has no physics component";
		}
	}

private:
	Keystate k;
	glm::vec3 f;
};