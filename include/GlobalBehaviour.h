#pragma once
#pragma once
#include "Behaviour.h"
class GameObject;
class Game;
class GlobalBehaviour :public Behaviour
{
public:
	GlobalBehaviour() {}
	virtual ~GlobalBehaviour() {}

	virtual void update(double) = 0;
	void setTarget(Game * x) { target = x; }
	Game * getTarget() { return target; }
protected:
	Game * target;

};