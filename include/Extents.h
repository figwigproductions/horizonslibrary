#pragma once
#define GLM_SWIZZLE_XYZW

#include <glm-0.9.9.2/glm/glm.hpp>

struct Extents
{
	Extents operator *(glm::mat4 mat)const
	{
		Extents e;
		e.minExtent = glm::vec3( mat * glm::vec4(minExtent,1));
		e.maxExtent = glm::vec3(mat * glm::vec4(maxExtent,1));
		e.centre = glm::vec3(mat * glm::vec4(centre,1));
		e.maxPoint = glm::vec3(mat * glm::vec4(maxPoint,1));
		e.minPoint = glm::vec3(mat * glm::vec4(minPoint,1));
		return e;
	}

	Extents operator-(glm::vec3 avg) const
	{
		Extents e;
		e.minExtent = minExtent - avg;
		e.minPoint = minPoint - avg;
		e.maxExtent = maxExtent - avg;
		e.maxPoint = maxPoint - avg;
		e.centre = centre - avg;
		return e;
	}


	glm::vec3 minExtent;
	glm::vec3 maxExtent;
	glm::vec3 centre;
	glm::vec3 maxPoint;
	glm::vec3 minPoint;
};