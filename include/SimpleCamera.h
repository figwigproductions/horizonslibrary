#pragma once
#include "Camera.h"
class RotatingCamera :
	public Camera
{
public:
	RotatingCamera();
	~RotatingCamera();

	void update(double delta, int mouseX, int mouseY, bool playing);

	glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos);

	void setTarget(glm::vec3 x) { cameraTarget = x; }

	void setPos(glm::vec3 x) { cameraPos = x; }

	void setSpeed(float x) { speed = x; }
private:
	
	
	float angle = 0.0f;

	float speed = 0.0001f;
	const float radius = 20.0f;
};



class StaticLookAtCamera : public Camera
{
public:
	StaticLookAtCamera(glm::vec3 position, GameObject * lookAt);
	~StaticLookAtCamera();

	void update(double delta, int mouseX, int mouseY, bool playing);

	glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos);

	void setTarget(GameObject *  x) { target = x; }

	void setPos(glm::vec3 x) { cameraPos = x; }

private:


	float angle = 0.0f;
	GameObject* target = nullptr;
};
