#include "PublicBehaviour.h"
class BrightnessPP;

#define FADE_SPLASH 0
#define FADE_FROM_BLACK 1

class Fade : public PublicBehaviour
{
public:
	Fade(int type);
	~Fade();

	void update(double);
	void startFade(double fadeTime);

	BrightnessPP * brightness;
	
private:
	void fadeSplash(double delta);
	void fadeIn(double delta);

	double timer;
	double maxTime;
	float maxBright = 1.0f;

	bool fadingIn = true;
	bool active = true;
	bool paused = false;
	
	int type;
};