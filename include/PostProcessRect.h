#pragma once
#include "ImageRect.h"
#include "Shader.h"
class PostProcessRect :
	public ImageRect
{
public:
	PostProcessRect(GLuint x);
	~PostProcessRect();

	void draw(RenderSettings*);
	void setShader(Shader * x) { postProcessingShader = x; }
private:
	Shader * postProcessingShader;
};

