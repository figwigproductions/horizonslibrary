#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Texture.h"

class FBOGroup
{
public:
	FBOGroup();
	~FBOGroup();

	int getTotalPasses() { return bufferCount; }
	void bindFBOByIndex(int i)
	{ 
		bindFrameBuffer(FBOs[i], dimensions[i * 2], dimensions[(i * 2) + 1]);
	}
	glm::mat4 getCameraAngleRequiredByIndex(int i) { return cameraAngles[i]; }
	glm::vec4 getClipPlaneByIndex(int i) { return clipPlanes[i]; }
	bool getClipPlaneUseBoolByIndex(int i) { return FBOs[i]; }

	void setCameraAngle(int i, glm::mat4 newAngle) { cameraAngles[i] = newAngle; }
	virtual void unBind() = 0;
	virtual void update() = 0;

	virtual Texture getTextureDetailsByIndex(int i) { return Texture(); }
protected:
	int bufferCount = 0;

	//Arrays to contain relevant FBOs
	static const int MAX_FBOS = 5;
	GLuint FBOs[MAX_FBOS] = { 0 };
	glm::mat4 cameraAngles[MAX_FBOS] = { glm::mat4(1)};
	glm::vec4 clipPlanes[MAX_FBOS] = { glm::vec4(0) };
	bool usePlanes[MAX_FBOS] = { false };
	int dimensions[MAX_FBOS * 2] = { 0 };

	virtual void bindFrameBuffer(GLuint buffer, int width, int height)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, buffer);
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}


};

