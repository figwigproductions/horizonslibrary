#pragma once
#include "Bounds.h"
#include "GameObject.h"
class GameObjectQuadtree
{
public:
	GameObjectQuadtree(Bounds2DXZ bounds, int level, GameObjectQuadtree * parent );
	~GameObjectQuadtree();


	void clear();
	void addGameObject(GameObject*);

	Bounds2DXZ getBounds() { return bounds; }

private:
	Bounds2DXZ bounds;

	void splitIntoNodes();
	void moveAllObjectsToNodes();
	void tryMoveObjectToNode(GameObject*);
	GameObjectQuadtree* parent =nullptr;
	int thisLevel = 0;
	
	GameObjectQuadtree* nodes[4] = { nullptr };

	bool split = true;

	int objectCount = 0;
	const static int MAX_OBJECTS_PER_NODE = 25;
	const static int MAX_LEVELS = 11;
	std::vector<GameObject*> objects;
	std::vector<GameObject*> objectsToRetain;
};

