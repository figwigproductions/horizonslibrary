#pragma once
class GameObject;

class Component
{
public:
	Component(GameObject * h) { host = h; }
	virtual ~Component() {}

	GameObject* getHost() { return host; }

protected:
	GameObject * host = nullptr;
};

