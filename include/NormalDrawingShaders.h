#pragma once
#include "Shader.h"
#include <CoreStructures\CoreStructures.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
class NormalDrawingShaders :
	public Shader
{
public:

	~NormalDrawingShaders();
	static NormalDrawingShaders* getInstance();

	void setMats(glm::mat4 T, glm::mat4 R,glm::mat4 S, glm::mat4 C);
	void setProj(glm::mat4);
private:
	NormalDrawingShaders();

	std::string vertFilename = "Shaders\\vertex_shader_normal.shader";
	std::string geometryFilename = "Shaders\\geometry_shader_normal.shader";
	std::string fragFilename = "Shaders\\fragment_shader_normal.shader";

	void setup();

	static NormalDrawingShaders* thisPointer;

	GLuint locTn;

	GLuint locRn;

	GLuint locSn;

	GLuint locCn;

	GLuint nLocProj;
};