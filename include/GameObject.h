#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include "Config.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "RenderSettings.h"
#include "Shader.h"
#include "PrivateBehaviour.h"
#include "ComponentReference.h"
#include "Extents.h"
#include "Volume.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/btBulletCollisionCommon.h>
#include <glm-0.9.9.2/glm/gtx/matrix_decompose.hpp>
#include "Object.h"

class GameObject : public Object
{
public:
	GameObject();


	virtual ~GameObject()
	{
		delete volume;
		for (int count = 0; count < MAX_BEHAVIOURS; count++)
		{
			if (privateBehaviours[count] != nullptr) delete privateBehaviours[count];
		}
	}

	virtual void update(double deltaTime);
	virtual void draw(RenderSettings*) = 0;

	//return the position
	glm::vec3 getPos();

	//return the rotation
	glm::vec3 getRot();

	glm::vec3 getScale();

	glm::mat4 getRotMat();

	glm::mat4 getTransMat();

	glm::mat4 getScaleMat();

	glm::mat4 getModelMat();

	virtual void setPos(glm::vec3 pos);

	void setRot(glm::vec3 pos);

	void setRot(glm::quat q);

	void setScale(glm::vec3 pos);

	void setConfig(Config newConfig) { thisConfig = newConfig;}

	Config getConfig() { return thisConfig; }

	Volume * getVolume() {	return volume;}

	btCollisionShape * getBtVolume() {return btVolume;}

	void setVolume(Volume * v) { volume = v; }
	void setVolume(btCollisionShape * v) { btVolume = v; }

	virtual void setParent(GameObject* newParent) { parent = newParent;  }

	virtual GameObject* getParent() { return parent; }

	void addPrivateBehaviour(PrivateBehaviour* x)
	{
		for (int count = 0; count < MAX_BEHAVIOURS; count++)
		{
			if (privateBehaviours[count] == nullptr)
			{
				privateBehaviours[count] = x;
				x->setTarget(this);
				x->setup();
				return;
			}
		}
	}

	void updateBehaviours(double delta)
	{
		for (int count = 0; count < MAX_BEHAVIOURS; count++)
		{
			if (privateBehaviours[count] != nullptr)

			{
				privateBehaviours[count]->update(delta);
			}
		}
	}

	VAOData * getVAOData() { return dataVAO; }

	//returns the extents timed by the scale and the transformation. Please fix to include rotation
	Extents getWorldExtents();
	Extents getScaledExtents();

	std::string name = "No Name Assigned";
	Extents e;
	void kill() { dead = true; }

protected:
	
	bool dead = false;

	VAOData * dataVAO =nullptr;

	Config thisConfig;

	GameObject * parent = nullptr;

	GLuint defaultShaders = 0;

	const static int MAX_BEHAVIOURS = 24;
	PrivateBehaviour * privateBehaviours[MAX_BEHAVIOURS] = { nullptr };

	Volume * volume = nullptr;
	btCollisionShape * btVolume = nullptr;


	glm::mat4 R = glm::toMat4(glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));
	glm::mat4 pR = glm::toMat4(glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));

	glm::mat4 T = glm::mat4();
	glm::mat4 pT = glm::translate(glm::mat4(), glm::vec3(0, 0, 0));

	glm::mat4 S = glm::mat4(1);

	bool modelMatChanged = true;
	glm::mat4 modelMat = glm::mat4();

	float rotX = 0.0f;
	float rotY = 0.0f;
	float rotZ = 0.0f;

};

