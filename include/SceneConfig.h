#pragma once
#include <string>
#include "TerrainTextureData.h"
struct SceneConfig
{
	std::string terrainKeyword = "Terrain";
	std::string emptyParentKeyword = "p_";
	std::string waypointKeyword = "wp_";
	std::string pathKeyword = "path_";


	std::string lightKeyword = "light_";

	std::string collisionVolumeKeyword = "cv_";
	std::string cylinderVolumeKeyword = "cyl_";
	std::string boxVolumeKeyword = "bx_";
	std::string planeVolumeKeyword = "pl_";
	std::string convexTriKeyword = "cnvx_";
	std::string hostlessKeyword = "hstlss_";
	std::string staticComplexKeyword = "static_";

	void addInstancible(std::string x)
	{
		instancible[actInstancible] = x;
		actInstancible++;
	}

	int getInstancible()
	{
		return actInstancible;
	}

	TerrainTextureData * texData;

	//scale of all of the objects
	float scale = 1.0f;

	bool fixInstancedObjectsOnTerrain = true;

	//how much to tile the terrain
	float tilingFactor = 10.0f;

	//edge length of the grid the terrain will be divided into
	int terrainDivisor = 1;

	int terrainCount = 1;

	const static int MAX_INSTANCIBLE = 64;
	int actInstancible = 0;
	std::string instancible[MAX_INSTANCIBLE] = { "" };
};
