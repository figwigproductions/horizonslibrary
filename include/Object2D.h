#pragma once
#include "GameObject.h"
#include <stdio.h>
#include <string>
#include <FreeImage\FreeImagePlus.h>
#include "texture_loader.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Bounds.h"

#define GUI_TRANSFORM_ANCHOR_TR 3
#define GUI_TRANSFORM_ANCHOR_TL 2
#define GUI_TRANSFORM_ANCHOR_BL 0
#define GUI_TRANSFORM_ANCHOR_BR 1
#define GUI_TRANSFORM_ANCHOR_CEN 4

class GUIObject;

struct GUITransform
{
	int anchorType = GUI_TRANSFORM_ANCHOR_BL;
private:
	glm::vec3 pos = glm::vec3(0);
	glm::vec2 scale  = glm::vec2(1);
	glm::vec2 relativeScale = glm::vec2(1);

public:
	glm::vec2 res = glm::vec2(50);
	glm::vec2 getScreenRes();
	glm::mat4 getProj();
	glm::vec3 getPos();
	glm::mat4 getScaleMat();
	glm::vec2 getScale();
	glm::vec2 getRelativeScale();

	glm::vec2 getlocalScale()
	{
		return scale;
	}

	void setScale(glm::vec2 x)
	{
		scale = x;
		relativeScale = x;
	}

	void setLocalScale(glm::vec2 c)
	{
		relativeScale = c;
	}

	void setPos(glm::vec3 p)
	{
		pos = p;
	}

	glm::mat4 getPosMat();

	Bounds2DXY getScreenBounds();
	bool isMouseOver(float x, float y);

	GUIObject* guiObjectRef = nullptr;
};



class GUIObject : public virtual GameObject
{
public:
	
	GUIObject();
	~GUIObject() {}
	virtual void draw(RenderSettings*) {}
	virtual void update(double delta) { deltaTime = delta; updateBehaviours(delta); }

	void setPos(glm::vec3 c) { transform.setPos(c); }
	void setScale(glm::vec3 c) { transform.setScale(c); }
	void setLocalScale(glm::vec3 c) { transform.setLocalScale(c); }

	GUITransform transform;

	GUIObject* getParent() { return parent; }

	void setParent(GUIObject* x) { parent = x; }

protected:

	GUIObject* parent = nullptr;
	//Texture containers
	GLuint texture = 0;
	double deltaTime;
};