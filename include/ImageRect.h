#pragma once
#include "Config.h"
#include "Object2D.h"
#include "Texture.h"

class ImageRect :
	public GUIObject
{
public:
	ImageRect(GLuint);
	ImageRect(GLuint, glm::vec2);
	ImageRect(Texture);
	ImageRect(glm::vec2);
	ImageRect();
	~ImageRect();

	void sortVAO();
	void draw(RenderSettings*);
	void setTexture(GLuint x) { texture = x; }
	void applyScaling();
	GLuint getTextureID() { return texture; }
protected:
	const int totalPoints = 8;

	float points[8] =
	{
		-1.0f,1.0f,
		1.0f,1.0f,
		1.0f,-1.0f,
		-1.0f,-1.0f
	};	

	float texts[8] =
	{
		0.0,1.0f,
		1.0f,1.0f,
		1.0f,0.0f,
		0.0f,0.0f
	};



	GLuint VAO = 0;

	GLuint pointsVBO = 0;
	GLuint textsVBO = 0;
};

