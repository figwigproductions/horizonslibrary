#pragma once
#include "GameObject.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/euler_angles.hpp>
#include "ShadowBox.h"

class Camera
{
public:
	Camera();
	virtual ~Camera() {}

	//Every Camera needs their own version of this call!
	virtual void update(double delta, int mouseX, int mouseY, bool playing) = 0;

	glm::mat4 getCameraMat() { return camera; }
	glm::mat4 getCameraMatNoPersp() { return cameraNoPersp; }
	glm::mat4 getPersp() { return persp; }
	glm::vec3 getCameraPos() { return cameraPos; }

	void setTarget(GameObject* newTarget) { target = newTarget; }

	virtual glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos) = 0;

	void setMouse(int x, int y) { diffX = x; diffY = y; }

	ShadowBox * getShadowBox() {
		if (thisShadowBox == nullptr) { thisShadowBox = new ShadowBox(this); } return thisShadowBox;
	}

	glm::vec2 getCameraAngle() { return cameraAngle; }
protected:
	void calcPersp();

	ShadowBox * thisShadowBox = nullptr;

	void reportPosition();

	int positionReportLoc = 0;

#pragma region CameraPosition Containers

	glm::vec3 cameraPos;
	glm::vec3 cameraTarget;
	glm::vec3 cameraDirection;
	glm::vec3 cameraRight;
	glm::vec3 cameraUp;
	glm::vec3 difference = glm::vec3(0.0f,0.0f,0.0f);

	glm::vec2 cameraAngle = glm::vec2(0.0f);

	glm::vec3 getCameraPosTarget(bool playing, GameObject* player);
	
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

	//The fixed default rotation of the camera
	glm::vec3 cameraRot = glm::vec3(0.0f, 0.0f, 0.0f);

	//The vector relative to the player that the camera wants to sit in 
	glm::vec3 posRelPlayer = glm::vec3(0.0f, 8.5f, -29.0f);
	glm::vec3 targetCameraPos = glm::vec3(0.0f, 0.0f, 0.0f);

	//The vector relative to the player that the camera wants to look at
	glm::vec3 posTrackRelRotPlayer = glm::vec3(0.0f, 4.0f, -5.0f);

	const float cameraSpeed = 0.73f;
	const float cameraRadius = 5.0f;

	int diffX = 0;
	int diffY = 0;

	float totalXRot = 0.0f;
	float totalYRot = 0.0f;

	float yDecay = 0.91f;
	float xDecay = 0.91f;


	float maxDist = 6000.0f;
	//Higher = lower
	int mouseSens = 120;

	glm::mat4 camera;
	glm::mat4 cameraNoPersp;
	glm::mat4 persp;
	GameObject * target;
#pragma endregion
};

