#pragma once
#include "FBO.h"
#include "Config.h"

class PostProcessingShader;

class PostProccessingFBO :
	public FBOGroup
{
public:
	PostProccessingFBO(Config thisConfig,float);
	~PostProccessingFBO();
	void unBind();
	void update();

	void blitTo(PostProccessingFBO*);

	void blitToScreen();
	void bind();

	GLuint getRenderFBO() { return renderFBO; }
	GLuint getTexture() { return texture; }

	Texture getTextureDetailsByIndex(int i) { return Texture(texture, glm::vec2(dimensions[0], dimensions[1]),"Post Processor"); }


	PostProcessingShader * shader = nullptr;
private:
	void setupFBO();
	
	Config thisConfig;
	GLuint renderFBO;

	float aaAmount = 0;

	GLuint texture = 0;
	GLuint depthBuffer = 0;
	GLuint colourBuffer = 0;
	GLuint rbo = 0;

};

